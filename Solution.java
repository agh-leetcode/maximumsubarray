class Solution {
    public int maxSubArray(int[] nums) {
        int resultMax = nums[0];
        int maxNow = nums[0];
        
        for(int i = 1; i < nums.length; i++){
            maxNow = Math.max(maxNow + nums[i], nums[i]);
            resultMax = Math.max(resultMax, maxNow);
        }
        return resultMax;
    }
}